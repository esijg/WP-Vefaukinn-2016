<!-- Header Starts -->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>
        <?php wp_title('|',1,'right'); ?> <?php bloginfo('name'); ?>
    </title>

    <?php wp_enqueue_script("jquery"); ?>
    <?php wp_head(); ?>

    <!-- Bootstrap -->
    <link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet">

    <link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/dynalight" type="text/css"/>
    <link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/railway-sans" type="text/css"/>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-offset-2 col-md-8 post">
                <h1><a class="brand" href="<?php echo site_url(); ?>"><?php bloginfo('name'); ?></a></h1>

                <p class="lead"><?php bloginfo('description'); ?></p>

                <ul><?php wp_list_pages(array('title_li' => '')); ?></ul>
            </div>
        </div>
    </div>
