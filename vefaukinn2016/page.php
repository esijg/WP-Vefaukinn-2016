<?php get_header(); ?>
<div class="container-fluid">
    <div class="row content" id="content">
		<div class="col-md-offset-2 col-md-8 post">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<h1><?php the_title(); ?></h1>
			  	<?php the_content(); ?>
			<?php endwhile; else: ?>
				<p><?php _e('Sorry, this page does not exist.'); ?></p>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
