<?php get_header(); ?>

<div class="container-fluid">
    <div class="row content" id="content">

<?php $previousDate = 'foo'; ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <div class="col-md-offset-2 col-md-8 post">

        <?php if ( $previousDate != get_the_date() || $previousDate == 'foo' ): ?>
            <h2 class="date"><?php the_date(); ?></h2>
        <?php endif; ?>

        <span class="author label label-default"><?php the_author_posts_link(); ?></span>
        <span class="post"><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></span>


        <?php $previousDate = get_the_date(); ?>

    </div>


<?php endwhile; ?>

    <div class="row" id="pagination">
        <div class="col-md-offset-2 col-md-8">
            <nav>
              <ul class="pager">
                <li class="previous"><?php next_posts_link( 'Eldri fréttir' ); ?></li>
                <li class="next"><?php previous_posts_link( 'Nýrri fréttir' ); ?></li>
              </ul>
            </nav>
        </div>
    </div>



    </div>
</div>

<?php else : ?>

    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>

<?php endif; ?>

<?php get_footer(); ?>
